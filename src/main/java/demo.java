import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class demo {
    WebDriver driver;

    @Before
    public void Before() {
        System.out.println("start");
        //khai bao webdriver
        System.setProperty("webdriver.chrome.driver", "src/test/resources/webdriver/Chrome/chromedriver.exe");
        // khoi tao webdriver
        driver = new ChromeDriver();
        //su dung phuong thuc voi webdriver
        driver.get("https://accounts.shopbase.com/sign-in");
        driver.manage().window().maximize();
    }

    @After
    public void After() {
        driver.quit();
        System.out.println("Fnish");
    }

    @Test
    public void Test() throws InterruptedException {
        System.out.println("Test");
        inputToElement("//input[@id='email']", "shopbase@beeketing.net");
        inputToElement("//input[@id='password']", ";|Xh!Hz&Hgh0/m{");
        clickElement("//button[@type='submit']");
        clickElement("(//span[@class='ui-login_domain-label'])[210]");
        clickElement("(//span[@class='unite-ui-dashboard__aside--text'])[5]");
        clickElement("(//button[@type='button'])[5]");
        inputToElement("//input[@placeholder='Short Sleeve T-Shirt']", "Name334");
        // inputToElement("//html[@style ='height: auto;']","Made in China");//
        inputToElement("//input[@id='price']", "20");
        inputToElement("//input[@id='compare_price']", "10");
        inputToElement("//input[@id='cost_price']", "10");
        inputToElement("//input[@placeholder='Product type']", "Shirt");
        inputToElement("//input[@placeholder=\"Nikola's Electrical Supplies\"]", "Hust");
        inputToElement("(//input[@type ='text'])[11]", "Tag1");
        clickElement("(//button[@type ='button'])[26]");
        Thread.sleep(10000);
        driver.get("https://au-ui-products.onshopbase.com/products/name334");
        driver.manage().window().maximize();

        check("Hust", msg("//p[@class='product__vendor mt0 mb12']//a"));

        check("NAME334", msg("//div[@class='product-page__gr-heading']//h3"));

        check("Shirt", msg("//p[@class='mb12']//a"));

        check("tag1", msg("//p[@class='mb0']//a"));

        check("$20.00 USD", msg("//div[@class='product__price mt0 mb12']"));


    }

    public void check(String str1, String str2) {
        Assert.assertEquals(str1, str2);
    }

    public String msg(String xpath) {
        waitElement(xpath);
        return getElementText(xpath);
    }

    public WebElement getElement(String xpath) {
        return driver.findElement((By.xpath(xpath)));
    }

    public void clickElement(String xpath) {
        waitElement(xpath);
        getElement(xpath).click();
    }

    public String getElementText(String xpath) {
        waitElement(xpath);
        return getElement(xpath).getText();
    }

    public void inputToElement(String xpath, String value) {
        waitElement(xpath);
        WebElement e = getElement(xpath);
        e.clear();
        e.sendKeys(value);
    }

    public void waitElement(String xpath) {
        WebDriverWait driverWait = new WebDriverWait(driver, 30);
        driverWait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(xpath)));
    }
}
